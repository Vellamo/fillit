### Fillit 
## Summary
This is our first Hive/42 project as a group. My partner and I decided it would be a good time to learn about binary! 
Although the "backend" uses binary, the algorithm itself is not very quick as it isn't "smart" when it attempts to place pieces.
If I had time, the algorithm should be overhauled to take this into consideration, preferably using recursion. At the time, we were more interested in moving on to bigger and better things than to worry about our final speed efficiency (which was already good enough for the best grade). 
Due to the use of binary however, at very very small sizes, the placement is blisteringly fast and it's unfortunate that our algorithm isn't as fast to showcase this better.

The program itself is composed of several functions, which each play a small part. Some information is obfuscated due to the usage of binary as a method of validation/"stamping". However we attempted to keep the number of magic numbers to a minimum, whilst adhering to the Norme (42/Hive code formatting rules).

## Usage
Run the Makefile. Use a Valid (or Invalid!) file (some are included in Test_Files) as an argument with fillit, ie.
`fillit Test_Files/4_Valid`
